﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Net.Mail;
using System.Text;
using System.Net;
using System.Configuration;
using System.Web.Security;

public partial class REMSCOPforum_ShareTopic : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
        string topicId = Request.QueryString["tid"];
        string topic = Server.UrlDecode(Request.QueryString["topic"]);

        string loginUserEmail = Membership.GetUser(User.Identity.Name).Email;

        //send email to LMCi
        string strTo = txtEmailto.Text.Trim();
        string strFrom = loginUserEmail;
        MailMessage objMailMsg = new MailMessage(strFrom, strTo);

        string[] emaillist = txtCC.Text.TrimEnd(';').Split(';');

        foreach(string email in emaillist)
        {
            if(!string.IsNullOrEmpty(email))
                 objMailMsg.CC.Add(email.Trim());
        }

        //objMailMsg.CC.Add("jwang@seiservices.com"); 

        objMailMsg.BodyEncoding = Encoding.UTF8;
        objMailMsg.Subject = txtSubject.Text;
        string message = edtBody.Content;
        message += "<p>=================================================================================</p>";
        //message += "<div>Topic: <h1>" + topic + "</h1></div>";
        message += "<p>Please click the follow link to join discuss:</p>";
        message += "<a href='http://rems.ed.gov/COP/Account/Login.aspx?ReturnUrl=../REMSCOPforum/topics.aspx?ForumID=" + topicId + "'>" + topic + "</a>"; ;
        objMailMsg.Body = message;
        objMailMsg.Priority = MailPriority.High;
        objMailMsg.IsBodyHtml = true;

        //prepare to send mail via SMTP transport
        SmtpClient objSMTPClient = new SmtpClient();
        objSMTPClient.Host = "mail2.seiservices.com";
        //NetworkCredential userCredential = new NetworkCredential("SEInfo@seiservices.com", "");
        //NetworkCredential userCredential = new NetworkCredential("mail2.seiservices.com", "");
        objSMTPClient.Credentials = CredentialCache.DefaultNetworkCredentials;
        objSMTPClient.Send(objMailMsg);

        ClientScriptManager csm = Page.ClientScript;
        csm.RegisterClientScriptBlock(this.GetType(), "", "<script>window.close();</script>");

       // ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "confirmation", "<script>alert('Thank you for submitting your data. ED will review your submission and contact you should anything need to be changes.');</script>", false);

    }
}