
$(document).ready(function(){
    // Activate Carousel
    $("#communitySpotlight").carousel("pause");

    // Go to the previous item
    $("a#backBtn").click(function(){
        $("#communitySpotlight").carousel("prev");
    });

    // Go to the next item
    $("a#fwdBtn").click(function(){
        $("#communitySpotlight").carousel("next");
    });
    

});
