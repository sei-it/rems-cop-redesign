﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class REMSCOPforum_remsUserinfo : System.Web.UI.Page
{
    private const string ASCENDING = " ASC";
    private const string DESCENDING = " DESC";

    public SortDirection GridViewSortDirection
    {
        get
        {
            if (ViewState["sortDirection"] == null)
                ViewState["sortDirection"] = SortDirection.Ascending;

            return (SortDirection)ViewState["sortDirection"];
        }
        set { ViewState["sortDirection"] = value; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {

    }
    string fileName = "REMSUserinfo";
    protected void ExportExcel(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "_Export.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            //To Export all pages
            gvReports.AllowPaging = false;

            gvReports.Visible = true;

            gvReports.HeaderRow.BackColor = System.Drawing.Color.White;
            foreach (System.Web.UI.WebControls.TableCell cell in gvReports.HeaderRow.Cells)
            {
                cell.BackColor = gvReports.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in gvReports.Rows)
            {
                row.BackColor = System.Drawing.Color.White;
                foreach (System.Web.UI.WebControls.TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = gvReports.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = gvReports.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            gvReports.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { } </style>";
            Response.Write(style);
            Response.Write(sw.ToString());
            Response.Flush();
            Response.End();
            //Response.Flush();
            //HttpContext.Current.ApplicationInstance.CompleteRequest();
            gvReports.Visible = false;
        }
    }

    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void grdvwlist_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdvwlist.PageIndex = e.NewPageIndex;
        grdvwlist.DataBind();
    }
    protected void grdvwlist_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortExpression = e.SortExpression;

        if (GridViewSortDirection == SortDirection.Ascending)
        {
            GridViewSortDirection = SortDirection.Descending;
            SortGridView(sortExpression, DESCENDING);
        }
        else
        {
            GridViewSortDirection = SortDirection.Ascending;
            SortGridView(sortExpression, ASCENDING);
        }   
    }

    private void SortGridView(string sortExpression, string direction)
    {
        ////  You can cache the DataTable for improving performance
        //DataTable dt = GetData().Tables[0];

        //DataView dv = new DataView(dt);
        //dv.Sort = sortExpression + direction;

        //grdvwlist.DataSource = dv;
        //grdvwlist.DataBind();
    }
}