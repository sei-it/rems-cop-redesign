﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;



[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="REMSforum")]
public partial class remsUsersDataClassesDataContext : System.Data.Linq.DataContext
{
	
	private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
	
  #region Extensibility Method Definitions
  partial void OnCreated();
  partial void InsertremsUser(remsUser instance);
  partial void UpdateremsUser(remsUser instance);
  partial void DeleteremsUser(remsUser instance);
  partial void InsertForumTopic(ForumTopic instance);
  partial void UpdateForumTopic(ForumTopic instance);
  partial void DeleteForumTopic(ForumTopic instance);
  partial void InsertCalendar(Calendar instance);
  partial void UpdateCalendar(Calendar instance);
  partial void DeleteCalendar(Calendar instance);
  #endregion
	
	public remsUsersDataClassesDataContext() : 
			base(global::System.Configuration.ConfigurationManager.ConnectionStrings["AspNetForumConnectionString"].ConnectionString, mappingSource)
	{
		OnCreated();
	}
	
	public remsUsersDataClassesDataContext(string connection) : 
			base(connection, mappingSource)
	{
		OnCreated();
	}
	
	public remsUsersDataClassesDataContext(System.Data.IDbConnection connection) : 
			base(connection, mappingSource)
	{
		OnCreated();
	}
	
	public remsUsersDataClassesDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
			base(connection, mappingSource)
	{
		OnCreated();
	}
	
	public remsUsersDataClassesDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
			base(connection, mappingSource)
	{
		OnCreated();
	}
	
	public System.Data.Linq.Table<remsUser> remsUsers
	{
		get
		{
			return this.GetTable<remsUser>();
		}
	}
	
	public System.Data.Linq.Table<ForumTopic> ForumTopics
	{
		get
		{
			return this.GetTable<ForumTopic>();
		}
	}
	
	public System.Data.Linq.Table<Calendar> Calendars
	{
		get
		{
			return this.GetTable<Calendar>();
		}
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.remsUsers")]
public partial class remsUser : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private System.Guid _Userid;
	
	private string _aspnet_UserName;
	
	private string _institutionType;
	
	private string _k12Role;
	
	private string _higherEDRole;
	
	private string _otherRoleDes;
	
	private string _FirstName;
	
	private string _LastName;
	
	private string _State;
	
	private string _City;
	
	private string _Email;
	
	private string _SelectedType;
	
	private System.Nullable<int> _yrExpEmergency;
	
	private System.Nullable<int> _yrExpED;
	
	private string _interests;
	
	private bool _isAccept;
	
	private System.DateTime _CreatedOn;
	
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnUseridChanging(System.Guid value);
    partial void OnUseridChanged();
    partial void Onaspnet_UserNameChanging(string value);
    partial void Onaspnet_UserNameChanged();
    partial void OninstitutionTypeChanging(string value);
    partial void OninstitutionTypeChanged();
    partial void Onk12RoleChanging(string value);
    partial void Onk12RoleChanged();
    partial void OnhigherEDRoleChanging(string value);
    partial void OnhigherEDRoleChanged();
    partial void OnotherRoleDesChanging(string value);
    partial void OnotherRoleDesChanged();
    partial void OnFirstNameChanging(string value);
    partial void OnFirstNameChanged();
    partial void OnLastNameChanging(string value);
    partial void OnLastNameChanged();
    partial void OnStateChanging(string value);
    partial void OnStateChanged();
    partial void OnCityChanging(string value);
    partial void OnCityChanged();
    partial void OnEmailChanging(string value);
    partial void OnEmailChanged();
    partial void OnSelectedTypeChanging(string value);
    partial void OnSelectedTypeChanged();
    partial void OnyrExpEmergencyChanging(System.Nullable<int> value);
    partial void OnyrExpEmergencyChanged();
    partial void OnyrExpEDChanging(System.Nullable<int> value);
    partial void OnyrExpEDChanged();
    partial void OninterestsChanging(string value);
    partial void OninterestsChanged();
    partial void OnisAcceptChanging(bool value);
    partial void OnisAcceptChanged();
    partial void OnCreatedOnChanging(System.DateTime value);
    partial void OnCreatedOnChanged();
    #endregion
	
	public remsUser()
	{
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Userid", DbType="UniqueIdentifier NOT NULL", IsPrimaryKey=true)]
	public System.Guid Userid
	{
		get
		{
			return this._Userid;
		}
		set
		{
			if ((this._Userid != value))
			{
				this.OnUseridChanging(value);
				this.SendPropertyChanging();
				this._Userid = value;
				this.SendPropertyChanged("Userid");
				this.OnUseridChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_aspnet_UserName", DbType="NVarChar(256) NOT NULL", CanBeNull=false)]
	public string aspnet_UserName
	{
		get
		{
			return this._aspnet_UserName;
		}
		set
		{
			if ((this._aspnet_UserName != value))
			{
				this.Onaspnet_UserNameChanging(value);
				this.SendPropertyChanging();
				this._aspnet_UserName = value;
				this.SendPropertyChanged("aspnet_UserName");
				this.Onaspnet_UserNameChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_institutionType", DbType="NVarChar(150)")]
	public string institutionType
	{
		get
		{
			return this._institutionType;
		}
		set
		{
			if ((this._institutionType != value))
			{
				this.OninstitutionTypeChanging(value);
				this.SendPropertyChanging();
				this._institutionType = value;
				this.SendPropertyChanged("institutionType");
				this.OninstitutionTypeChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_k12Role", DbType="NVarChar(150)")]
	public string k12Role
	{
		get
		{
			return this._k12Role;
		}
		set
		{
			if ((this._k12Role != value))
			{
				this.Onk12RoleChanging(value);
				this.SendPropertyChanging();
				this._k12Role = value;
				this.SendPropertyChanged("k12Role");
				this.Onk12RoleChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_higherEDRole", DbType="NVarChar(150)")]
	public string higherEDRole
	{
		get
		{
			return this._higherEDRole;
		}
		set
		{
			if ((this._higherEDRole != value))
			{
				this.OnhigherEDRoleChanging(value);
				this.SendPropertyChanging();
				this._higherEDRole = value;
				this.SendPropertyChanged("higherEDRole");
				this.OnhigherEDRoleChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_otherRoleDes", DbType="NVarChar(MAX)")]
	public string otherRoleDes
	{
		get
		{
			return this._otherRoleDes;
		}
		set
		{
			if ((this._otherRoleDes != value))
			{
				this.OnotherRoleDesChanging(value);
				this.SendPropertyChanging();
				this._otherRoleDes = value;
				this.SendPropertyChanged("otherRoleDes");
				this.OnotherRoleDesChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_FirstName", DbType="NVarChar(100)")]
	public string FirstName
	{
		get
		{
			return this._FirstName;
		}
		set
		{
			if ((this._FirstName != value))
			{
				this.OnFirstNameChanging(value);
				this.SendPropertyChanging();
				this._FirstName = value;
				this.SendPropertyChanged("FirstName");
				this.OnFirstNameChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_LastName", DbType="NVarChar(100)")]
	public string LastName
	{
		get
		{
			return this._LastName;
		}
		set
		{
			if ((this._LastName != value))
			{
				this.OnLastNameChanging(value);
				this.SendPropertyChanging();
				this._LastName = value;
				this.SendPropertyChanged("LastName");
				this.OnLastNameChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_State", DbType="NVarChar(50)")]
	public string State
	{
		get
		{
			return this._State;
		}
		set
		{
			if ((this._State != value))
			{
				this.OnStateChanging(value);
				this.SendPropertyChanging();
				this._State = value;
				this.SendPropertyChanged("State");
				this.OnStateChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_City", DbType="NVarChar(200)")]
	public string City
	{
		get
		{
			return this._City;
		}
		set
		{
			if ((this._City != value))
			{
				this.OnCityChanging(value);
				this.SendPropertyChanging();
				this._City = value;
				this.SendPropertyChanged("City");
				this.OnCityChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Email", DbType="NVarChar(50)")]
	public string Email
	{
		get
		{
			return this._Email;
		}
		set
		{
			if ((this._Email != value))
			{
				this.OnEmailChanging(value);
				this.SendPropertyChanging();
				this._Email = value;
				this.SendPropertyChanged("Email");
				this.OnEmailChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_SelectedType", DbType="NVarChar(50)")]
	public string SelectedType
	{
		get
		{
			return this._SelectedType;
		}
		set
		{
			if ((this._SelectedType != value))
			{
				this.OnSelectedTypeChanging(value);
				this.SendPropertyChanging();
				this._SelectedType = value;
				this.SendPropertyChanged("SelectedType");
				this.OnSelectedTypeChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_yrExpEmergency", DbType="Int")]
	public System.Nullable<int> yrExpEmergency
	{
		get
		{
			return this._yrExpEmergency;
		}
		set
		{
			if ((this._yrExpEmergency != value))
			{
				this.OnyrExpEmergencyChanging(value);
				this.SendPropertyChanging();
				this._yrExpEmergency = value;
				this.SendPropertyChanged("yrExpEmergency");
				this.OnyrExpEmergencyChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_yrExpED", DbType="Int")]
	public System.Nullable<int> yrExpED
	{
		get
		{
			return this._yrExpED;
		}
		set
		{
			if ((this._yrExpED != value))
			{
				this.OnyrExpEDChanging(value);
				this.SendPropertyChanging();
				this._yrExpED = value;
				this.SendPropertyChanged("yrExpED");
				this.OnyrExpEDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_interests", DbType="NVarChar(2000)")]
	public string interests
	{
		get
		{
			return this._interests;
		}
		set
		{
			if ((this._interests != value))
			{
				this.OninterestsChanging(value);
				this.SendPropertyChanging();
				this._interests = value;
				this.SendPropertyChanged("interests");
				this.OninterestsChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_isAccept", DbType="Bit NOT NULL")]
	public bool isAccept
	{
		get
		{
			return this._isAccept;
		}
		set
		{
			if ((this._isAccept != value))
			{
				this.OnisAcceptChanging(value);
				this.SendPropertyChanging();
				this._isAccept = value;
				this.SendPropertyChanged("isAccept");
				this.OnisAcceptChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CreatedOn", DbType="DateTime NOT NULL")]
	public System.DateTime CreatedOn
	{
		get
		{
			return this._CreatedOn;
		}
		set
		{
			if ((this._CreatedOn != value))
			{
				this.OnCreatedOnChanging(value);
				this.SendPropertyChanging();
				this._CreatedOn = value;
				this.SendPropertyChanged("CreatedOn");
				this.OnCreatedOnChanged();
			}
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.ForumTopics")]
public partial class ForumTopic : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private int _TopicID;
	
	private int _ForumID;
	
	private int _UserID;
	
	private string _Subject;
	
	private bool _Visible;
	
	private int _LastMessageID;
	
	private int _IsSticky;
	
	private bool _IsClosed;
	
	private int _ViewsCount;
	
	private int _RepliesCount;
	
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnTopicIDChanging(int value);
    partial void OnTopicIDChanged();
    partial void OnForumIDChanging(int value);
    partial void OnForumIDChanged();
    partial void OnUserIDChanging(int value);
    partial void OnUserIDChanged();
    partial void OnSubjectChanging(string value);
    partial void OnSubjectChanged();
    partial void OnVisibleChanging(bool value);
    partial void OnVisibleChanged();
    partial void OnLastMessageIDChanging(int value);
    partial void OnLastMessageIDChanged();
    partial void OnIsStickyChanging(int value);
    partial void OnIsStickyChanged();
    partial void OnIsClosedChanging(bool value);
    partial void OnIsClosedChanged();
    partial void OnViewsCountChanging(int value);
    partial void OnViewsCountChanged();
    partial void OnRepliesCountChanging(int value);
    partial void OnRepliesCountChanged();
    #endregion
	
	public ForumTopic()
	{
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_TopicID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
	public int TopicID
	{
		get
		{
			return this._TopicID;
		}
		set
		{
			if ((this._TopicID != value))
			{
				this.OnTopicIDChanging(value);
				this.SendPropertyChanging();
				this._TopicID = value;
				this.SendPropertyChanged("TopicID");
				this.OnTopicIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ForumID", DbType="Int NOT NULL")]
	public int ForumID
	{
		get
		{
			return this._ForumID;
		}
		set
		{
			if ((this._ForumID != value))
			{
				this.OnForumIDChanging(value);
				this.SendPropertyChanging();
				this._ForumID = value;
				this.SendPropertyChanged("ForumID");
				this.OnForumIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_UserID", DbType="Int NOT NULL")]
	public int UserID
	{
		get
		{
			return this._UserID;
		}
		set
		{
			if ((this._UserID != value))
			{
				this.OnUserIDChanging(value);
				this.SendPropertyChanging();
				this._UserID = value;
				this.SendPropertyChanged("UserID");
				this.OnUserIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Subject", DbType="NVarChar(255) NOT NULL", CanBeNull=false)]
	public string Subject
	{
		get
		{
			return this._Subject;
		}
		set
		{
			if ((this._Subject != value))
			{
				this.OnSubjectChanging(value);
				this.SendPropertyChanging();
				this._Subject = value;
				this.SendPropertyChanged("Subject");
				this.OnSubjectChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Visible", DbType="Bit NOT NULL")]
	public bool Visible
	{
		get
		{
			return this._Visible;
		}
		set
		{
			if ((this._Visible != value))
			{
				this.OnVisibleChanging(value);
				this.SendPropertyChanging();
				this._Visible = value;
				this.SendPropertyChanged("Visible");
				this.OnVisibleChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_LastMessageID", DbType="Int NOT NULL")]
	public int LastMessageID
	{
		get
		{
			return this._LastMessageID;
		}
		set
		{
			if ((this._LastMessageID != value))
			{
				this.OnLastMessageIDChanging(value);
				this.SendPropertyChanging();
				this._LastMessageID = value;
				this.SendPropertyChanged("LastMessageID");
				this.OnLastMessageIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IsSticky", DbType="Int NOT NULL")]
	public int IsSticky
	{
		get
		{
			return this._IsSticky;
		}
		set
		{
			if ((this._IsSticky != value))
			{
				this.OnIsStickyChanging(value);
				this.SendPropertyChanging();
				this._IsSticky = value;
				this.SendPropertyChanged("IsSticky");
				this.OnIsStickyChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IsClosed", DbType="Bit NOT NULL")]
	public bool IsClosed
	{
		get
		{
			return this._IsClosed;
		}
		set
		{
			if ((this._IsClosed != value))
			{
				this.OnIsClosedChanging(value);
				this.SendPropertyChanging();
				this._IsClosed = value;
				this.SendPropertyChanged("IsClosed");
				this.OnIsClosedChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ViewsCount", DbType="Int NOT NULL")]
	public int ViewsCount
	{
		get
		{
			return this._ViewsCount;
		}
		set
		{
			if ((this._ViewsCount != value))
			{
				this.OnViewsCountChanging(value);
				this.SendPropertyChanging();
				this._ViewsCount = value;
				this.SendPropertyChanged("ViewsCount");
				this.OnViewsCountChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_RepliesCount", DbType="Int NOT NULL")]
	public int RepliesCount
	{
		get
		{
			return this._RepliesCount;
		}
		set
		{
			if ((this._RepliesCount != value))
			{
				this.OnRepliesCountChanging(value);
				this.SendPropertyChanging();
				this._RepliesCount = value;
				this.SendPropertyChanged("RepliesCount");
				this.OnRepliesCountChanged();
			}
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}

[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Calendars")]
public partial class Calendar : INotifyPropertyChanging, INotifyPropertyChanged
{
	
	private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
	
	private int _ID;
	
	private System.Nullable<int> _ContentGroupID;
	
	private string _WholeTitle;
	
	private string _CalendarTitle;
	
	private string _CalendarDescription;
	
	private System.Nullable<System.DateTime> _CalendarStart;
	
	private System.Nullable<System.DateTime> _CalendarEnd;
	
	private string _TimeZone;
	
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnContentGroupIDChanging(System.Nullable<int> value);
    partial void OnContentGroupIDChanged();
    partial void OnWholeTitleChanging(string value);
    partial void OnWholeTitleChanged();
    partial void OnCalendarTitleChanging(string value);
    partial void OnCalendarTitleChanged();
    partial void OnCalendarDescriptionChanging(string value);
    partial void OnCalendarDescriptionChanged();
    partial void OnCalendarStartChanging(System.Nullable<System.DateTime> value);
    partial void OnCalendarStartChanged();
    partial void OnCalendarEndChanging(System.Nullable<System.DateTime> value);
    partial void OnCalendarEndChanged();
    partial void OnTimeZoneChanging(string value);
    partial void OnTimeZoneChanged();
    #endregion
	
	public Calendar()
	{
		OnCreated();
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
	public int ID
	{
		get
		{
			return this._ID;
		}
		set
		{
			if ((this._ID != value))
			{
				this.OnIDChanging(value);
				this.SendPropertyChanging();
				this._ID = value;
				this.SendPropertyChanged("ID");
				this.OnIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ContentGroupID", DbType="Int")]
	public System.Nullable<int> ContentGroupID
	{
		get
		{
			return this._ContentGroupID;
		}
		set
		{
			if ((this._ContentGroupID != value))
			{
				this.OnContentGroupIDChanging(value);
				this.SendPropertyChanging();
				this._ContentGroupID = value;
				this.SendPropertyChanged("ContentGroupID");
				this.OnContentGroupIDChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_WholeTitle", DbType="NVarChar(1000)")]
	public string WholeTitle
	{
		get
		{
			return this._WholeTitle;
		}
		set
		{
			if ((this._WholeTitle != value))
			{
				this.OnWholeTitleChanging(value);
				this.SendPropertyChanging();
				this._WholeTitle = value;
				this.SendPropertyChanged("WholeTitle");
				this.OnWholeTitleChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CalendarTitle", DbType="VarChar(50)")]
	public string CalendarTitle
	{
		get
		{
			return this._CalendarTitle;
		}
		set
		{
			if ((this._CalendarTitle != value))
			{
				this.OnCalendarTitleChanging(value);
				this.SendPropertyChanging();
				this._CalendarTitle = value;
				this.SendPropertyChanged("CalendarTitle");
				this.OnCalendarTitleChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CalendarDescription", DbType="VarChar(1000)")]
	public string CalendarDescription
	{
		get
		{
			return this._CalendarDescription;
		}
		set
		{
			if ((this._CalendarDescription != value))
			{
				this.OnCalendarDescriptionChanging(value);
				this.SendPropertyChanging();
				this._CalendarDescription = value;
				this.SendPropertyChanged("CalendarDescription");
				this.OnCalendarDescriptionChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CalendarStart", DbType="DateTime")]
	public System.Nullable<System.DateTime> CalendarStart
	{
		get
		{
			return this._CalendarStart;
		}
		set
		{
			if ((this._CalendarStart != value))
			{
				this.OnCalendarStartChanging(value);
				this.SendPropertyChanging();
				this._CalendarStart = value;
				this.SendPropertyChanged("CalendarStart");
				this.OnCalendarStartChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CalendarEnd", DbType="DateTime")]
	public System.Nullable<System.DateTime> CalendarEnd
	{
		get
		{
			return this._CalendarEnd;
		}
		set
		{
			if ((this._CalendarEnd != value))
			{
				this.OnCalendarEndChanging(value);
				this.SendPropertyChanging();
				this._CalendarEnd = value;
				this.SendPropertyChanged("CalendarEnd");
				this.OnCalendarEndChanged();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_TimeZone", DbType="VarChar(5)")]
	public string TimeZone
	{
		get
		{
			return this._TimeZone;
		}
		set
		{
			if ((this._TimeZone != value))
			{
				this.OnTimeZoneChanging(value);
				this.SendPropertyChanging();
				this._TimeZone = value;
				this.SendPropertyChanged("TimeZone");
				this.OnTimeZoneChanged();
			}
		}
	}
	
	public event PropertyChangingEventHandler PropertyChanging;
	
	public event PropertyChangedEventHandler PropertyChanged;
	
	protected virtual void SendPropertyChanging()
	{
		if ((this.PropertyChanging != null))
		{
			this.PropertyChanging(this, emptyChangingEventArgs);
		}
	}
	
	protected virtual void SendPropertyChanged(String propertyName)
	{
		if ((this.PropertyChanged != null))
		{
			this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
#pragma warning restore 1591
