﻿<%@ Page Language="C#" CodeBehind="COPindex.aspx.cs" AutoEventWireup="true" Inherits="aspnetforum.COPindex" MasterPageFile="~/remsForumMasterPage.Master" %>

<asp:Content ContentPlaceHolderID="ContentMain" ID="AspNetForumContent" runat="server">
<div class="remsCOP-content">
				<section class="cop-log-home">
					<div class="container row clearfix">
						<div class="container ad">
							<div class="grid_12 omega">
								<h2>Welcome to the Community of Practice (CoP)!</h2> </div>
							<!-- /grid_12 omega -->
							<div class="grid_12 omega">
								<p>The CoP is a collaborative community that enhances the ability of schools, school districts, IHEs, state education agencies (SEAs), and their community partners to develop high-quality emergency operations plans (EOPs) and implement comprehensive emergency management planning efforts.</p>
							</div>
							<!-- /grid_12 omega -->
							<div class="grid_12 omega">
								<div class="btn-wrap grid_12 omega">
									<p class="pad-top">Start or enter a discussion in the:</p>
									        <asp:Panel ID="pnlForumList" runat="server">
                                            </asp:Panel>
								</div>
							</div>
						</div>
						<!-- /container -->
					</div>
					<!-- /container row CoP clearfix -->
				</section>
				<section class="community-huddles">
					<div class="container row clearfix">
						<div class="container ad">
							<div class="grid_12 omega">
								<div class="grid_1 omega"> <i class="fa fa-map-marker"></i> </div>
								<div class="grid_11 omega">
									<h2>Our Private Groups are now called Community Huddles!</h2> </div>
								<div class="grid_12 omega">
									<div class="community-btn-wrap">
										<p class="pad-top">Community Huddles allow you to create or join conversations and topics specific to smaller, more focused groups within the nationwide Community of Practice.</p>
										<ul>
											<%--<li><a class="btn btn-info btn-lg col-xs-12" href="default.aspx?gid=<%# getHuddleID() %>">VIEW EXISTING COMMUNITY HUDDLES</a></li>--%>
											<li><a class="btn btn-info btn-lg col-xs-12" href="#" onclick="ShowMessage();">START MY OWN COMMUNITY HUDDLE</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
    <script type="text/javascript">
        function ShowMessage() { alert("To start your own Community Huddle, email us at info@remstacenter.org with a suggested title, description and list of invited participants."); }
    
    </script>
</asp:Content>